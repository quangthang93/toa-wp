<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'toa' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<|+C*1Jw54J;N;EwgF M8A_eG-:7dF7UOa?=IU7a)(OUyXPgrOS_7)0#jsNx2f$O' );
define( 'SECURE_AUTH_KEY',  'Svk<S#H*-/Dr1,%)#IFPIfc0RhnXsbw<wx[7W+@=!;*p(!sL`{0>KOLu30MHPCy#' );
define( 'LOGGED_IN_KEY',    'O7Pxy0)dM5?apt%W3&KMf@^_+^<mlPLMFWm+jc2OEh]8[6UB_5OFk]H?BF*=nLbp' );
define( 'NONCE_KEY',        '1(qKd76L)e,^}r_|y:?JwMkv~ Y6G^6(]=cu3ox3w|gd$y:{-8_Qh69q|gK *:~@' );
define( 'AUTH_SALT',        '3crY==gU*#s$wzdg/w}NJ1#.stanOr,,+-C#}g$wyyHrxI{?s6v~8-@m$ ]mLr@m' );
define( 'SECURE_AUTH_SALT', 'O9&YgF1bYqVIPdHV~$./uO#`l#> D,X *7X6l}N8jZ$ 9FhMRDLfy~=P#-~%{LR^' );
define( 'LOGGED_IN_SALT',   'I#7UH&CQy)xIAWA~/rM/wwr|{ _3Z/=rcTt`zdsx>V`hg+mjo7TK:-~3t;MHWy#r' );
define( 'NONCE_SALT',       'v1pp>666R8QP70H: Yi2kEN5n6a^/}_o_%gAHx<i#zps5S~;z~ @Yr#g.e]mSX=w' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
