<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'xs234504_wp1' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'xs234504_wp1' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', '6vt5ho39nx' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0YD%Q%+)0wkZ?xBID4(sd)D|r lFF!oaT=5)@},EBT1H)/^r  _uh]Z3l|^Mz=RR' );
define( 'SECURE_AUTH_KEY',  's2s?*iSUD[.l V>>3FNMd$Ewu(5)pbIfYG@>^[0G`)QT)xv];u-Y~MyY`Gmu8{)x' );
define( 'LOGGED_IN_KEY',    '^_f@TA%I@q+M%W_=j1WU{M`Tm@-)/rRImPm*~,Gc*U`Bq&)[tfl9J55`L+X(5j[k' );
define( 'NONCE_KEY',        'h<@(M/HsDFz_q~2O3|d}>{K@c47LZgz1)9bzpx(Q|YtJDifkuA#PQ6bvvet>8bi+' );
define( 'AUTH_SALT',        'q,J p%:v$33pQ/4w!W(d57bQrSvLHdGl[$1HKz)dp_K4L*tWyH:/MtSDIQ=v_Fmq' );
define( 'SECURE_AUTH_SALT', '{a;snACw3FGI>K>kon_qD6)]}AaT_Uku60Wv=;eu~uCc,M_^8Kf_qtC;$mYQA[,M' );
define( 'LOGGED_IN_SALT',   'vE%9CX 7&5Ys)</uo)L<EgjbmRmm%^QY8VNPf<g<=(QaLTBa|#oO1^t@h6UyEyK%' );
define( 'NONCE_SALT',       '$XR6nJtDD@`PIUQV8_UJjv.-4ef6`rTI$3/mFpJ]KI,O&AEm]6H V-cfB_&K!Ukg' );
define( 'WP_CACHE_KEY_SALT','@H_ w{[TZ[!1^UQ%2,x))eIdI=I)Sz05z>wONn2fQ^*7n%dLN}GHYF0!@VNfMcT]' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


/** Dont show error message */
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);

