<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
  <div class="p-end p-booking">
    <div class="section-title idx">
      <p class="en">contact</p>
      <h2>お問い合わせ</h2>
    </div>
    <div class="contact-content">
      <div class="v-contact">
        <div class="u-layout-smaller type2 u-pv-4">
          <div class="c-steps">
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">1</span>
              <p></p>
              <p class="c-steps__col__label">お客様情報を入力</p>
              <p></p>
            </div>
            <div class="c-steps__col is-active">
              <span class="c-steps__col__number u-font-rajdhani">2</span>
              <p></p>
              <p class="c-steps__col__label">内容確認</p>
              <p></p>
            </div>
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">3</span>
              <p></p>
              <p class="c-steps__col__label">完了</p>
              <p></p>
            </div>
          </div>
          <p class="contact-content--guide">入力内容をご確認ください。 入力内容を編集する場合は「修正する」、 <br>お問い合わせを送る場合は「送信する」をクリックしてください。</p>
          <?php the_content(); ?>
        </div>
      </div><!-- ./v-contact -->
    </div><!-- ./contact-content -->
  </div>
</main><!-- ./main -->
<script>
  $( "button.btn-gray" ).click( function () {
    AjaxZip3.zip2addr( 'zip', null, 'pref', 'address' );
    return false;
  } );
</script>
<?php
get_footer();
