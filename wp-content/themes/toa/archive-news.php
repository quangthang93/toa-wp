<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main --idx">
    <div class="banner-idx">
      <picture>
        <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
      </picture>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul>
          <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
          <li>お知らせ</li>
        </ul>
      </div>
    </div>
    <div class="section-title idx">
      <p class="en">news</p>
      <h2>お知らせ</h2>
    </div>
    <?php
      $args[ 'post_type' ]     = 'news';
      $args[ 'posts_per_page' ]  = 16;
      $args[ 'post_status' ]     = 'publish';
      $args[ 'orderby' ]       = 'date';
      $args[ 'order' ]       = 'DESC';
      $paged             = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
      $args[ 'paged' ]       = $paged;
      $loop  = null;
      $loop  = new WP_Query( $args );
    ?>
    <div class="news pt-30">
      <div class="container">
        <div class="news__inner">
          <div class="news__listWrap">
            <ul class="news__list">
              <?php 
                while ( $loop->have_posts() ) : $loop->the_post(); 
                $news_cat = get_the_terms(get_the_ID(), 'category');
              ?>

                <li class="news__item">
                  <a href="<?php the_permalink()?>" class="news__item-inner">
                    <div class="news__item-dateWrap">
                      <span class="date"><?php the_time( 'Y.m.d' ); ?></span>
                      <!-- <span class="tag">お知らせ</span> -->
                      <?php 
                        if ( $news_cat && ! is_wp_error( $news_cat ) ) :
                          foreach ($news_cat as $news_cat_data):
                            if($news_cat_data->term_id) {
                                printf('<span class="tag">%s</span>', $news_cat_data->name);
                            }
                            break;
                          endforeach;
                        else: 
                          echo '<span class="tag">お知らせ</span>';
                        endif;?>   
                    </div>
                    <p class="desc"><?php the_title(); ?></p>
                  </a>
                </li>
            
              <?php endwhile; ?>
            </ul>
          </div>
          <?php echo pagination( $loop, 4, 16 ) ?>
        </div>
      </div>
    </div>
  </main><!-- ./main -->

<?php
get_footer();
