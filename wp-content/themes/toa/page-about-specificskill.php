<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
          <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
          <li><a href="<?php echo home_url(); ?>/about-technical-interns/">よくあるご質問</a></li>
					<li>特定技能外国人の受入れについてのご質問</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>特定技能外国人の受入れ<br class="sp-only" />についてのご質問</h2>
		</div>
		<div class="question light">
			<div class="container">
				<div class="question__inner">
					<p class="question__desc">当組合で外国人実習生の受入れをお手伝いさせていただく過程において、よくご質問されることや受入れ前の疑問や不安に思われることをまとめました。このページにないご質問がございましたら<a class="link-blue" href="<?php echo home_url(); ?>/contact">こちらまでお問い合わせ</a>ください。</p>
					<ul class="question__list">
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q1</span>
								<span class="title">特定技能制度ってなんですか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A1</span>
									<span class="text">人手不足に対応するため、生産性向上･国内人材確保の取組みをしてもなお人材確保が困難な状況にある特定の業種において、一定の専門性･技能を有し即戦力となる外国人を受入れる制度です。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q2</span>
								<span class="title">どんな業種が受入れることができるのですか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A2</span>
									<span class="text">現状、下記の14分野です。<br>介護、ビルクリニーング、素形材産業、産業機械製造業、電気･電子情報関連産業、建設、造船･舶用工業、自動車整備、航空、宿泊、農業、漁業、飲食料品製造業、外食業</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q3</span>
								<span class="title">どこの国の人を紹介してもらえますか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A3</span>
									<span class="text">ベトナム国のみの受入れとなります。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q4</span>
								<span class="title">長く働いてもらえるの?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A4</span>
									<span class="text">現在、特定技能1号は上限が5年です。<br>特定技能1号から特定技能2号に移行対象の業種は、建設、造船･舶用工業のみとなっています。特定技能2号は、上限を設けられていません。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q5</span>
								<span class="title">日本語は、わかりますか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A5</span>
									<span class="text">生活に支障のない程度の語学力がある人材を派遣いたします。特定産業分野ごとに業務上必要な日本語能力は試験等で勉強しますが、入国後の支援が必要です。TOA協同組合もサポートいたしますが、身近な受入れ先企業のサポートが一番大切です。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q6</span>
								<span class="title">特定技能と技能実習の違いは、なんですか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A6</span>
									<span class="text">技能実習は、技能、技術又は知識の開発途上国等への移転を図り、開発途上国等の経済発展を担う「人づくり」に協力することを目的とするとされており国際協力の推進です。<br>特定技能は、A1のとおり人手不足に対応する制度です。<br>また、技能実習は通常監理団体と送出機関によって人材紹介が行われており、人数枠も条件が設定されています。特定技能は、監理団体や送出機関を通さずに直接採用することが可能で人数枠の制限もありません。<br>転職については、技能実習は原則できませんが、特定技能は同一の業務区分内、または試験により技能水準の共通性が確認されている業務区分間においては可能となっています。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q7</span>
								<span class="title">現在、外国人技能実習生を受入れています。<br />引き続き長く働いてもらいたいと思っています。特定技能で働いてもらうことができますか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A7</span>
									<span class="text">技能実習２号を良好に修了された実習生は、特定技能１号の試験等は免除され法務省の定める移行対象職種がございます。現在の職種名と作業名が分かれば移行の分野をお伝えさせていただくことができます。<br>また、技能実習の２号修了者と特定活動で在留中の方には、移行の特例措置が2019年９月末までごさいますので、詳細は直接お問い合わせください。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q8</span>
								<span class="title">以前、当社で働いていた技能実習生に特定技能でまた働いてもらいたいと思っています。どうしたらいいですか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A8</span>
									<span class="text">ベトナム国の方でしたらTOA協同組合で対応が可能です。詳細はお問い合わせフォームからお問い合わせください。[<a href="<?php echo home_url(); ?>/contact" class="link-blue">お問い合わせはこちら</a>]</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q9</span>
								<span class="title">雇用条件は、どのようになりますか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A9</span>
									<span class="text">日本人と同等以上です。</span>
								</p>
							</div>
						</li>
						<li>
							<a href="javascript:void(0)" class="question__head js-accorLabel">
								<span class="number">Q10</span>
								<span class="title">住居は会社で準備しなければならないのですか?</span>
							</a>
							<div class="js-accorCnt">
								<p class="question__answer">
									<span class="alpha">A10</span>
									<span class="text">適切な住居の確保に係る支援として、次のいずれかを行うことが求められます。<br>①１号特定技能外国人が賃借人として賃貸借契約を締結するに当たり、不動産仲介事業社や賃貸物件に係る情報を提供し、必要に応じて当該外国人に同行し、住居探しの補助を行う。賃貸借契約に際し連帯保証人が必要な場合であって、連帯保証人として適当な者がいない場合、下記は特定技能所属機関が実施します。<br>　・特定技能所属機関が連帯保証人となる<br>　・利用可能な家賃債務保証業者を確保するとともに、特定技能所属機関等が緊急連絡先となる<br><br>②特定技能所属機関等が自ら賃借人となって賃貸借契約を締結した上で、１号特定技能外国人の合意の下、当該外国人に対して住居して提供する<br><br>③特定技能所属機関等が所有する社宅等を、１号特定技能外国人の合意の下、当該外国人に対して住居として提供すると、運用要領別冊に定められていますので補助または、提供をお願いします。</span>
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<?php
get_footer();
