<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/skills.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>お客様の声</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <h2>お客様の声</h2>
  </div>
  <div class="corporation-post">
    <div class="container">
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例01</span>
            <span class="text">家具製造</span>
          </p>
        </div>
        <h3>作業効率と現場の若返りのトライアルの一環として実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">生産ラインの作業者の高齢化が進んだため、作業効率と現場の若返りのトライアルの一環として実習生を導入。ベトナムに木工椅子製品の生産委託メーカーがあり、製品図面等の読解や通訳としての活用を今後検討中。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">がんばっている</p>
            <p>まず、語学力の問題についてです。まだまだ勉強不足の部分はありますが、覚える努力は認められます。現状、実務には全く問題点はありませんが、今回の導入人数が１名のみであるための影響があるかもしれません。勤務では前向きに実習を行っているのがはっきりと分かります。年齢が若いので実務が心配だったが、丁寧な仕事をしている。少し過剰に品質をこだわる部分はあるが、実務の中で経験として習得していく事として今後期待しています。１名のみの導入予定だが、年齢的にも彼の精神面が心配しています。本人にて解決が必要である部分が多く、現状では問題だと感じていません。</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例02</span>
            <span class="text">樹脂成型製造</span>
          </p>
        </div>
        <h3>生産ライン作業者の平均年齢の高齢化、品質向上を検討し実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">生産ラインのオペレ−タ−の平均年齢が高齢化し、求人で募集したが人材が集まらないため。また、生産ラインの若返り、品質の向上を検討し導入。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">想定以上であった。</p>
            <p>語学力に問題があるのではないかと不安でしたが、彼女たちの努力によりほぼ解決しています。また、日本人従業員との人間関係や年齢差がとても不安でしたが、全く問題は発生していません。なぜなら、彼女たちが生産ラインの日本人オペレーターとの日本語の会話を積極的に行い解決しているからです。自動車部品がメインであるため、品質・コスト・配送を高いレベルで要求されるが、彼女たち二人はミスがないため安心して任しています。さらに他の製品よりも難易度が高い高品質な部品を安心して任せることができます。</p>
            <p class="note">更に女性追加1名決定済み</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例03</span>
            <span class="text">金属素形材製造</span>
          </p>
        </div>
        <h3>ベトナムへ進出し、現地オペレーター・ラインリーダーの人材を確保するため導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">ベトナムへ進出し現在、現地工場が稼働中。現地オペレーター育成及びラインリーダも育成すべくエンジニアも合わせて導入。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">ベトナム人は個人のスキルが高い</p>
            <p>男性7名・女性3名の合計10名が、入国後、配属前にフォークリフト・クレーン・球掛けの免許取得が必要なため、1ヵ月間日本で講習を受けました。日本語研修の中で、静岡県内の教習所で通常講習を含め全員が一発合格しました。また、上記試験は国家試験であるため、日本語で行われます。ベトナム人とういう特別な待遇で取得した人が今までいませんが、日本人と同じ条件で取得しました。女性3名も一発合格し、現在、実習実務で活用しています。上記により、ベトナム人は基本的なスキルが高い認識を持ちました。</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例04</span>
            <span class="text">樹脂圧縮成型</span>
          </p>
        </div>
        <h3>日本人の従業員募集や派遣社員を雇用していたが、社員定着率が非常に低いため技能実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">導入前は日本人の従業員募集や派遣社員を雇用していましたが、製品の重量が重く、樹脂の圧縮成形加工の5Kの職場であり熱が発生し熱いという悪条件があるため、職場定着率が非常に低いとう問題に悩まされていました。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">我慢強い・礼儀正しい</p>
            <p>ベトナム人で体が大きく、体力のありそうな３名を導入し、試験的に雇用してみました。現状、工場が熱い中がんばっています。まだ評価をする段階ではありませんが、「はい。分かりました。」と返答し、何事にも熱心に取組んでいます。</p>
            <p class="note">更に男性追加1名決定済み</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例05</span>
            <span class="text">プラスチック成形</span>
          </p>
        </div>
        <h3>ベトナム人実習生を導入実績がある協力工場から高評価を聞き、中国からベトナムへ受入れ国を変更し導入を決意。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">現在、中国人６名の実習生を導入しているが、協力工場にてベトナム人実習生を導入し成果を上げていることを協力工場からの監査にて助言を受けました。協力工場の役員にベトナム人実習生の高評価を聞き、実習生の受入れ国の中国からベトナムに変更をしました。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">現場が明るくなり・効率も上がった。</p>
            <p>現在、先行３名のみの入国実習ではあるが彼女たちは非常に明るく、経営トップである社長の毎、月曜日の現場巡回においてもお父さんと呼び現場に明るさが一段と増していると思われる。入国後の実務に対しての評価は全く問題点は発生していない。実務的には、日本語に対してレベルが高く、作業内容の指示がしやすいと判断をしているが、今後の実務内容を見ながら評価をしていきたいと考える。</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<?php
get_footer();
