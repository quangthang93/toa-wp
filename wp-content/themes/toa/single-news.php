<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>
<?php 
  $PATH= get_template_directory_uri();
?>
<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li><a href="<?php echo home_url(); ?>/news">お知らせ</a></li>
        <li><?php the_title() ?></li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <p class="en">news</p>
    <h2>お知らせ</h2>
  </div>
  <div class="news">
    <div class="container">
      <div class="p-news--detail">
        <div class="p-news--detail-head">
          <span class="date"><?php the_time( 'Y.m.d' ); ?></span>
          <!-- <span class="tag">#お知らせ</span> -->
          <?php 
            $news_cat = get_the_terms(get_the_ID(), 'category');
            if ( $news_cat && ! is_wp_error( $news_cat ) ) :
              foreach ($news_cat as $news_cat_data):
                if($news_cat_data->term_id) {
                    printf('<span class="tag">%s</span>', $news_cat_data->name);
                }
                break;
              endforeach;
            else: 
              echo '<span class="tag">お知らせ</span>';
            endif;?>   
        </div>
        <div class="p-news--detail-cnt">
          <h1 class="p-news--detail-ttl"><?php the_title(); ?></h1>
          <div class="no-reset">
            <?php the_content(); ?>
          </div>
          <div class="btn-view-moreWrap">
            <a href="<?php echo home_url(); ?>/news" class="btn-view-more"><span>お知らせ一覧に戻る</span></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php endwhile; ?>
  <?php endif; ?>
</main><!-- ./main -->
<?php
get_footer();