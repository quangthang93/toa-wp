<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>404</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <h2>404 NOT FOUND</h2>
  </div>
  <div class="container">
    <p class="title-lv3 mt-50 mb-20">申し訳ございません。お探しのページは見つかりませんでした。</p>
    <p class="desc">お客様がアクセスしようとしたページは、 掲載期間が終了し削除されたか、別の場所に移動した可能性があります。<br>メニューから引き続き当社ホームページをご覧くださいませ。</p>
  </div>
</main><!-- ./main -->

<?php
get_footer();
