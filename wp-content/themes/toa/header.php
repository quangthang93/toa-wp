<?php 
  $PATH= get_template_directory_uri();
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>
    <?php 
      if (is_front_page()) {
        echo get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
      } else {
        wp_title('');
      }
    ?> 
    </title>
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
  <meta name="description" content="ベトナム人の受入れをお考えなら、TOA協同組合へお任せください。開発途上国と培った確かなネットワークで外国人労働者の受入れと人材育成・国際貢献をしっかりサポートいたします。">
  <meta property="og:site_name" content="TOA協同組合">
  <meta property="og:title" content="TOA協同組合 | 外国人の受入れをお考えなら、TOA協同組合へお任せください。">
  <meta property="og:description" content="ベトナム人の受入れをお考えなら、TOA協同組合へお任せください。開発途上国と培った確かなネットワークで外国人労働者の受入れと人材育成・国際貢献をしっかりサポートいたします。">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo home_url(); ?>">
  <meta property="og:image" content="<?php echo $PATH;?>/assets/images/ogp.jpg">
</head>

<body>
  <div class="wrapper">
    <header class="header">
      <div class="header__top">
        <div class="container">
          <div class="header__top-inner">
            <p class="header__top-address animated fadein left">外国人技能実習生受入管理団体 許可  許2108000023</p>
            <a class="header__top-sns pc-only animated fadein right" href="https://www.facebook.com/toakumiai" target="_blank">フェイスブック</a>
          </div>
        </div>
      </div>
      <div class="header__bottom" id="header-fixed">
        <div class="container">
          <div class="header__bottom-inner animated fadein up delay-3">
            <div class="header__logo">
              <h1><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/h-logo.svg" alt="TOA協同組合" /></a></h1>
            </div>
            <div class="header__right">
              <div class="header__contact">
                <div class="header__contact-item phone">
                  <a href="tel:0661554114">06-6155-4114</a>
                  <span class="pc-only">【受付時間：平日9：00〜18：00】</span>
                </div>
                <div class="header__contact-item email">
                  <a href="/contact/">お問い合わせ</a>
                  <span class="pc-only">【24時間受付中】</span>
                </div>
                <div class="header__contact-item facebook sp-only">
                  <a href="https://www.facebook.com/toakumiai" target="_blank">facebook</a>
                </div>
              </div>
              <button type="button" class="header__btn --menu sp-only">
                <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-menu.svg"/>
              </button>
              <nav class="header__nav pc-only">
                <ul class="header__nav-list">
                  <li>
                    <a href="javascript:void(0)">外国人技能実習生の受入れ</a>
                    <div class="dropdown">
                      <ul>
                        <li>
                          <a href="<?php echo home_url(); ?>/foreign-interns-1/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-1.jpg"/>
                            <span>外国人技能実習制度とは</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/foreign-interns-2/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-2.jpg"/>
                            <span>安心のサポート体制</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/foreign-interns-3/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-3.jpg"/>
                            <span>実習生受入れまでの流れ</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/foreign-interns-4/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-4.jpg"/>
                            <span>受入れのメリット</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <a href="javascript:void(0)">特定技能外国人の受入れ</a>
                    <div class="dropdown">
                      <ul>
                        <li>
                          <a href="<?php echo home_url(); ?>/specific-skilles-1/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-5.jpg"/>
                            <span>在留資格「特定技能」とは</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/specific-skilles-2/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-6.jpg"/>
                            <span>特定技能外国人受入れまでの流れ</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/specific-skilles-3/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-7.jpg"/>
                            <span>外国人の受入れをトータルサポート</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="<?php echo home_url(); ?>/voice/">お客様の声</a></li>
                  <li><a href="<?php echo home_url(); ?>/overview/">組合概要</a></li>
                  <li>
                    <a href="javascript:void(0)">よくあるご質問</a>
                    <div class="dropdown">
                      <ul>
                        <li>
                          <a href="<?php echo home_url(); ?>/about-technical-interns/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-8.jpg"/>
                            <span>外国人技能実習生の受入れについてのご質問</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo home_url(); ?>/about-specificskill/">
                            <img src="<?php echo $PATH;?>/assets/images/common/sub-menu-9.jpg"/>
                            <span>特定技能外国人の受入れについてのご質問</span>
                          </a>
                        </li>
                      </ul>
                  </li>
                  <li><a href="<?php echo home_url(); ?>/news/">お知らせ</a></li>
                </ul>
              </nav>
              <nav class="header__nav-sp">
                <div class="header__nav-sp__top">
                  <a><img src="<?php echo $PATH;?>/assets/images/common/h-logo-menu.svg" alt="TOA協同組合" /></a>
                  <button type="button" class="header__btn --close sp-only">
                    <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-close-wh.svg"/>
                  </button>
                </div> 
                <div class="header__nav-content">
                  <ul class="header__nav-list">
                    <li><a href="<?php echo home_url(); ?>">トップページ</a></li>
                    <li>
                      <a href="javascript:void(0)" class="js-accorLabel">外国人技能実習生の受入れ</a>
                      <ul class="header__nav-list__sub js-accorCnt">
                        <li><a href="<?php echo home_url(); ?>/foreign-interns-1/">外国人技能実習制度とは</a></li>
                        <li><a href="<?php echo home_url(); ?>/foreign-interns-2/">安心のサポート体制</a></li>
                        <li><a href="<?php echo home_url(); ?>/foreign-interns-3/">実習生受入れまでの流れ</a></li>
                        <li><a href="<?php echo home_url(); ?>/foreign-interns-4/">受入れのメリット</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="javascript:void(0)" class="js-accorLabel">特定技能外国人の受入れ</a>
                      <ul class="header__nav-list__sub js-accorCnt">
                        <li><a href="<?php echo home_url(); ?>/specific-skilles-1/">在留資格「特定技能」とは</a></li>
                        <li><a href="<?php echo home_url(); ?>/specific-skilles-2/">特定技能外国人受入れまでの流れ</a></li>
                        <li><a href="<?php echo home_url(); ?>/specific-skilles-3/">外国人の受入れをトータルサポート</a></li>
                      </ul>
                    </li>
                    <li><a href="<?php echo home_url(); ?>/voice/">お客様の声</a></li>
                    <li><a href="<?php echo home_url(); ?>/overview/">組合概要</a></li>
                    <li>
                      <a href="javascript:void(0)" class="js-accorLabel">よくあるご質問</a>
                        <ul class="header__nav-list__sub js-accorCnt">
                          <li><a href="<?php echo home_url(); ?>/about-technical-interns/">外国人技能実習生の受入れについてのご質問</a></li>
                          <li><a href="<?php echo home_url(); ?>/about-specificskill/">特定技能外国人の受入れについてのご質問</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo home_url(); ?>/news/">お知らせ</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header><!-- ./header -->

