<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

  <main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
          <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="<?php echo home_url(); ?>/foreign-interns-1/">外国人技能実習生の受入れ</a></li>
					<li>受入れのメリット</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>受入れのメリット</h2>
		</div>
		<div class="trainees-benefits">
			<div class="container">
				<div class="trainees-benefits__item">
					<div class="trainees-benefits__img">
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit1-1.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit1-2.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit1-3.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit1-4.jpg" alt="" class="cover">
							</picture>
						</p>
					</div>
					<div class="trainees-benefits__cnt">
						<h3>事業を通じて国際社会に貢献</h3>
						<p>人材育成を目的としたこの制度を通じて、友好的な諸外国との国際貢献につながります。<br>また、国際交流・貢献を行っている事業所としてイメージアップを図ることができます。</p>
					</div>
				</div>
				<div class="trainees-benefits__item">
					<div class="trainees-benefits__img">
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit2-1.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit2-2.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit2-3.jpg" alt="" class="cover">
							</picture>
						</p>
					</div>
					<div class="trainees-benefits__cnt">
						<h3>事業所・職員の活性化</h3>
						<p>労働意欲に溢れた技能実習生が職場に入ることで、新しい価値観や刺激を取り入れることができ、事業所の職員にも良い影響を及ぼし職場の活性化につながります。</p>
					</div>
				</div>
				<div class="trainees-benefits__item">
					<div class="trainees-benefits__img">
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit3-1.jpg" alt="" class="cover">
							</picture>
						</p>
						<p>
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/benefit3-2.jpg" alt="" class="cover">
							</picture>
						</p>
					</div>
					<div class="trainees-benefits__cnt">
						<h3>意欲ある優秀な人材の育成サポート</h3>
						<p>厳しい選考基準をクリアした技能実習生たちは現場での活躍が母国から期待されています。 送り出し機関と連携し、優秀な人材の育成を行うことで経済発展を担う「人づくり」に寄与することができます。</p>
					</div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->
<?php
get_footer();
