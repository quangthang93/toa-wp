<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
          <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="<?php echo home_url(); ?>/foreign-interns-1/">外国人技能実習生の受入れ</a></li>
					<li>安心のサポート体制</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>安心のサポート体制</h2>
		</div>
		<div class="trainees-support">
			<div class="container">
				<div class="col3">
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-1.png" alt="">
							</div>
							<h3>優秀なベトナム人の<br />通訳チーム</h3>
							<p>日本での実習中や職場などの日常生活で生じる言葉の問題の解消を行います。さらに、精神的なケアを行うことで技能実習生を全力でサポートします。また、現地の面接や企業訪問時の通訳などもサポートいたします。</p>
						</div>
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-2.png" alt="">
							</div>
							<h3>TOA協同組合スタッフの<br />トータルサポート</h3>
							<p>企業に制度のご説明や受入れ準備、各種手続きのご案内をさせていただきます。また、実習期間中には技能実習生に対して監査・訪問指導を行い企業をサポートいたします。</p>
						</div>
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-3.png" alt="">
							</div>
							<h3>トラブル発生時の<br />迅速な対応</h3>
							<p>サポートスタッフ全員が常時連絡可能で、急なトラブルにも迅速に対応いたします。また、技能実習生に通訳チーム全員の連絡先を伝えることで、気軽に相談できる環境を作り、未然にトラブルが回避できる体制を整えています。</p>
						</div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->
<?php
get_footer();
