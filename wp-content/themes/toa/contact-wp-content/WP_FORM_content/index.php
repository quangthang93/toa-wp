<div class="c-form">
<div class="c-form__row"><label class="c-form__row__label" for="company"><span class="c-form__row__label__text">貴社名</span>
</label>
<div class="c-form__row__field">[mwform_text name="company" id="company" class="c-form__input" size="60" placeholder="例) 株式会社ABC"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="department"><span class="c-form__row__label__text">部署</span></label>
<div class="c-form__row__field">[mwform_text name="department" id="department" class="c-form__input" size="60" placeholder="例) 人事部"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="name"><span class="c-form__row__label__text">お名前</span> <span class="c-form__required">必須</span> </label>
<div class="c-form__row__field">[mwform_text name="name" id="name" class="c-form__input" size="60" placeholder="例) 山田太郎"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="name2"><span class="c-form__row__label__text">フリガナ</span> <span class="c-form__required">必須</span> </label>
<div class="c-form__row__field">[mwform_text name="name2" id="name2" class="c-form__input" size="60" placeholder="例) ヤマダタロウ"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="zip"><span class="c-form__row__label__text">郵便番号</span></label>
<div class="c-form__row__field">[mwform_text name="zip" id="zip" class="c-form__input is-short" size="60" placeholder="例) 5640051" show_error="false"]<button class="btn-gray active is-short u-pc-ml-2 is-hide-confirm">住所を自動入力</button> [mwform_error keys="zip"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="pref"><span class="c-form__row__label__text">都道府県</span></label>
<div class="c-form__row__field">
<div class="c-form__select">[mwform_select name="pref" class="c-form__select__field" children=":選択してください,北海道,青森県,岩手県,宮城県,秋田県,山形県,福島県,茨城県,栃木県,群馬県,埼玉県,千葉県,東京都,神奈川県,山梨県,長野県,新潟県,富山県,石川県,福井県,岐阜県,静岡県,愛知県,三重県,滋賀県,京都府,大阪府,兵庫県,奈良県,和歌山県,鳥取県,島根県,岡山県,広島県,山口県,徳島県,香川県,愛媛県,高知県,福岡県,佐賀県,長崎県,熊本県,大分県,宮崎県,鹿児島県,沖縄県" post_raw="true" show_error="false"]</div>
[mwform_error keys="pref"]

</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="address"><span class="c-form__row__label__text">住所</span></label>
<div class="c-form__row__field">[mwform_text name="address" id="address" class="c-form__input" size="60" placeholder="例) 吹田市豊津町9-15"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="address2"><span class="c-form__row__label__text">建物名</span> </label>
<div class="c-form__row__field">[mwform_text name="address2" id="address2" class="c-form__input" size="60" placeholder="例) 日本興業ビル803"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="phone"><span class="c-form__row__label__text">電話番号</span> <span class="c-form__required">必須</span> </label>
<div class="c-form__row__field">[mwform_text name="phone" id="phone" class="c-form__input" size="60" placeholder="例) 0310001000"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="email"><span class="c-form__row__label__text">メールアドレス</span> <span class="c-form__required">必須</span> </label>
<div class="c-form__row__field">[mwform_email name="email" id="email" class="c-form__input" size="60" placeholder="例) example@xxxx.co.jp"]</div>
</div>
<div class="c-form__row">

<label class="c-form__row__label" for="content"><span class="c-form__row__label__text">お問い合わせ内容</span><span class="c-form__required">必須</span></label>
<div class="c-form__row__field">[mwform_textarea name="content" id="content" class="c-form__textarea" cols="50" rows="5" placeholder="入力してください"]</div>
</div>
<ul class="c-contact__action">
 	<li>[mwform_backButton class="c-contact__action__button c-button is-gray type2" value="修正する"]</li>
 	<li>[mwform_submitButton name="btnConfirm" class="c-contact__action__button c-button is-yellow mx-0 js-btn-alert" confirm_value="確認画面へ" submit_value="送信する"]</li>
</ul>
</div>