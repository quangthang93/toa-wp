<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

  <main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
          <li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="<?php echo home_url(); ?>/foreign-interns-1/">外国人技能実習生の受入れ</a></li>
					<li>実習生受入れまでの流れ</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>実習生受入れまでの流れ</h2>
		</div>
		<div class="about-diagram align-center pt-60 pb-60">
			<div class="container">
				<picture>
					<img src="<?php echo $PATH;?>/assets/images/common/diagram.png" alt="">
				</picture>
			</div>
		</div>
	</main><!-- ./main -->

<?php
get_footer();
