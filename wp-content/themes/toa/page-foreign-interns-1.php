<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

  <main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="<?php echo home_url(); ?>"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="<?php echo home_url(); ?>/foreign-interns-1/">外国人技能実習生の受入れ</a></li>
					<li>外国人技能実習制度とは</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>外国人技能実習制度とは</h2>
		</div>
		<div class="trainees-about">
			<div class="container">
				<div class="col2">
						<div class="col2-item">
							<div class="trainees-about__img">
								<picture>
									<img src="<?php echo $PATH;?>/assets/images/common/trainees-1.jpg" alt="">
								</picture>
							</div>
						</div>
						<div class="col2-item">
							<div class="trainees-about__cnt">
								<p>
                開発途上国の18歳以上の人材を企業と組合が一丸となり、日本の産業界に技能実習生として受入れ、日本で培われた技能、技術や知識を開発途上地域へ移転を図ります。<br>また、日常生活において日本の伝統文化や道徳理念を学習し、開発途上地域の経済発展を担う「人づくり」に協力することを目的として創設された制度です。
								</p>
								<!-- <h3 class="section-title-line base">外国人技能実習制度受入れ要件</h3> -->
								<div class="view-more-wrap mt-40">
									<a href="<?php echo $PATH;?>/pdf/kigyo.pdf"class="btn-read-file download" target="_blank"><span>一般職用 PDFダウンロード</span></a>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<?php
get_footer();
