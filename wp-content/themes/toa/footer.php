<?php 
  $PATH= get_template_directory_uri();
?>
<footer class="footer">
      <div class="footer__contact">
        <div class="footer__contact-top">
          <div class="container">
            <div class="footer__contact-top__inner">
              <p>まずはお気軽にご相談ください。<br class="sp-only" />相談無料協力支援団体　</p>
              <p></p><span class="btn">相談無料</span>
            </div>
          </div>
        </div>
        <div class="footer__contact-bottom">
          <div class="container">
            <p class="title">ご不明な点がございましたら、<br class="sp-only" />お気軽にお問い合わせください。</p>
            <div class="col2">
              <div class="col2-item sp-mb-30">
                <div class="footer__contact__inner">
                  <p class="block phone">
                    <strong>お電話でお問いわせ</strong>
                    <small>受付時間：平日9：00〜18：00</small>
                  </p>
                  <a href="tel:0661554114" class="number">06-6155-4114</a>
                </div>
              </div>
              <div class="col2-item">
                <div class="footer__contact__inner">
                  <p class="block email">
                    <strong>メールでお問いわせ</strong>
                    <small>24時間受け付けております。</small>
                  </p>
                  <a href="/contact/" class="btn contact"><span>お問い合わせはこちら</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer__fnavi">
        <div class="container">
          <div class="col3">
            <div class="col3-item">
              <div class="footer__fnavi__title">
                <p>外国人技能実習生の受入れ</p>
              </div>
              <ul class="footer__fnavi__list">
                <li><a href="<?php echo home_url(); ?>/foreign-interns-1/" class="line">外国人技能実習制度とは</a></li>
                <li><a href="<?php echo home_url(); ?>/foreign-interns-2/" class="line">安心のサポート体制</a></li>
                <li><a href="<?php echo home_url(); ?>/foreign-interns-3/" class="line">実習生受入れまでの流れ</a></li>
                <li><a href="<?php echo home_url(); ?>/foreign-interns-4/" class="line">受入れのメリット</a></li>
              </ul>
            </div>
            <div class="col3-item">
              <div class="footer__fnavi__title">
                <p>特定技能外国人の受入れ</p>
              </div>
              <ul class="footer__fnavi__list">
                <li><a href="<?php echo home_url(); ?>/specific-skilles-1/" class="line">在留資格「特定技能」とは</a></li>
                <li><a href="<?php echo home_url(); ?>/specific-skilles-2/" class="line">特定技能外国人受入れまでの流れ</a></li>
                <li><a href="<?php echo home_url(); ?>/specific-skilles-3/" class="line">外国人の受入れをトータルサポート</a></li>
              </ul>
            </div>
            <div class="col3-item">
              <div class="footer__fnavi__title">
                <p>よくあるご質問</p>
              </div>
              <ul class="footer__fnavi__list">
                <li><a href="<?php echo home_url(); ?>/about-technical-interns/" class="line">外国人技能実習生の受入れについてのご質問</a></li>
                <li><a href="<?php echo home_url(); ?>/about-specificskill/" class="line">特定技能外国人の受入れについてのご質問</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer__address">
        <div class="container">
          <div class="col2">
            <div class="col2-item px-0">
              <div class="footer__logo">
                <a href="/"><img src="<?php echo $PATH;?>/assets/images/common/h-logo.svg" width="156" height="109"alt="TOA協同組合" /></a>
              </div>
              <address>
                <p class="icon circle build">株式会社TOA協同組合</p>
                <p class="icon circle location">〒564-0051 大阪府吹田市豊津町9-15　日本興業ビル803</p>
                <p class="icon circle tel">TEL.06-6155-4114 ／ FAX.06-6155-4115</p>
              </address>
              <p><a href="https://www.facebook.com/toakumiai" class="btn sns-fb" target="_blank"><span>フェイスブック</span></a></p>
            </div>
            <div class="col2-item pc-pl-40">
              <div class="footer__map gg-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d204.86940328523707!2d135.49660770936117!3d34.75784849874473!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16e535d355f1c4c1!2zVE9B5Y2U5ZCM57WE5ZCI!5e0!3m2!1sja!2sjp!4v1640230158591!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
      <p class="copyright">&copy; TOA Coop. All Rights Reserved.</p>
    </footer><!-- ./footer -->
  </div>
  <?php wp_footer(); ?>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/tfunc.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script type='text/javascript' src='https://ajaxzip3.github.io/ajaxzip3.js?ver=5.6.6' id='ajaxzip3-js'></script>
  <script>
    jQuery(function ($) {
      $("button.btn-gray").click(function () {
        AjaxZip3.zip2addr('zip', null, 'pref', 'address');
        return false;
      });
    });
    </script>
</body>

</html>