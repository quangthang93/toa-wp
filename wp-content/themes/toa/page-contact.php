<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TOA
 */

get_header();
?>

<?php 
  $PATH= get_template_directory_uri();
?>

<main class="main">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <p class="en">contact</p>
    <h2>お問い合わせ</h2>
  </div>
  <div class="contact-content">
    <div class="container">
      <p class="contact-content--ttl phone"><span>お電話でのお問い合わせ</span></p>
      <div class="contact-content--phoneBox">
        <p class="contact-content--phoneBox-ttl phone">お電話にてお問い合わせ頂く場合はこちらまでご連絡ください。</p>
        <p class="contact-content--phoneBox-num">
          TEL: <a class="link" href="tel:06-6155-4114">06-6155-4114</a>
        </p>
        <p class="contact-content--phoneBox-time">[受付時間] 平日 9:00～18:00</p>
      </div>
      <p class="contact-content--ttl"><span>メールフォームでの<br class="sp-only2">お問い合わせ</span></p>
    </div>
    <div class="v-contact">
      <div class="u-layout-smaller u-pv-4">
        <div class="c-steps">
          <div class="c-steps__col is-active">
            <span class="c-steps__col__number u-font-rajdhani">1</span>
            <p></p>
            <p class="c-steps__col__label">問い合わせ内容入力</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">2</span>
            <p></p>
            <p class="c-steps__col__label">内容確認</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">3</span>
            <p></p>
            <p class="c-steps__col__label">完了</p>
            <p></p>
          </div>
        </div>
        <p class="contact-content--guide">お問い合わせは必要事項をご記入の上、下記フォームよりお送り下さい。<br> 送信内容を確認させていただき担当よりご連絡致します。</p>
        
        <?php the_content(); ?>

      </div>
    </div><!-- ./v-contact -->
  </div><!-- ./contact-content -->
</main><!-- ./main -->

<?php
get_footer();
